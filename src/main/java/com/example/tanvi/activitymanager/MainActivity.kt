package com.example.tanvi.activitymanager

import android.app.ActivityManager
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast


class MainActivity : AppCompatActivity() {
    lateinit var activityManager: ActivityManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        Toast.makeText(this, "isAppRunningOnLowRam\t" + activityManager.isLowRamDevice, Toast.LENGTH_SHORT).show()
        getInfoAboutRecentApps()

    }

    fun getInfoAboutRecentApps() {
        val recentTasks = activityManager.getRunningTasks(Integer.MAX_VALUE)

        for (i in recentTasks.indices) {
            Toast.makeText(this@MainActivity, recentTasks[i].baseActivity.toShortString() + "\t\t ID:" + recentTasks[i].id, Toast.LENGTH_SHORT).show()
        }

    }
}
